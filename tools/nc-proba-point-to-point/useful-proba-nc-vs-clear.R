library(ggplot2)
library(sqldf)

proba <- read.csv("useful-proba-nc-vs-clear.csv")
proba2 <- sqldf("
  select already_known, clear_proba as proba, 'Clear' as type from proba
  union
  select already_known, nc_proba as proba, 'Coding' as type from proba
")
ggplot(proba2, aes(x=already_known, y=proba, colour=type)) +
  geom_point() +
  geom_line() +
  scale_y_continuous(labels=scales::percent) +
  geom_vline(xintercept = 15, color="purple") +
  annotate("text", x = 13.5, y = 0.75, label = "75%", colour="purple", size=8) +
  annotate("text", x = 13.5, y = 0.05, label = "< 1%", colour="purple", size=8) +
  labs(y='Useless probability', x='Messages known by target', colour='Type', title=expression('Source knows n=20 msgs (on 𝔽'[2^8]*')')) +
  #scale_y_log10() +
  theme_minimal(base_size = 26)

