'use strict'

const Twit = require('twit')
const T = new Twit({
  consumer_key:         'REDACTED',
  consumer_secret:      'REDACTED',
  access_token:         'REDACTED',
  access_token_secret:  'REDACTED',
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
  strictSSL:            true,     // optional - requires SSL certificates to be valid.
})

const stream = T.stream('statuses/filter', { track: '#ARICHELLA' })

let counter = 1
let elapsed = 0
let start = new Date()

stream.on('tweet', (tweet) => {
  counter += 1
})

setInterval(() => {
  elapsed += 10
  console.log(counter, elapsed, counter / elapsed)
}, 10000)
