#include <iostream>
#include <queue>
#include <vector>
#include <map>
#include <random>
#include <deque>
#include <algorithm>
#include <limits>

using namespace std;

enum message_type { push, local, pull, reply };


class message {
  public:
   uint64_t sent_at;
   uint64_t received_at;
   uint64_t source;
   uint64_t target;
   uint64_t ttl;
   message_type type;
   uint64_t id;
   deque<uint64_t> history;
   deque<uint64_t> missing;

   bool operator<(const message &m1);
   message copy();
};

bool message::operator<(const message &m1) {
  return this->received_at < m1.received_at;
}

message message::copy() {
  message res;
  res.sent_at = this->sent_at;
  res.received_at = this->received_at;
  res.source = this->source;
  res.target = this->target;
  res.ttl = this->target;
  res.type = this->type;
  res.id = this->id;
  res.history = this->history;
  res.missing = this->missing;

  return res;
}

class node {
  public:
    mt19937 rng;
    uint64_t id;
    uint8_t fanout;
    uint8_t ttl;
    uint64_t size;
    uint8_t window_size;
    uint64_t pull_delta;
    deque<uint64_t> missing;
    deque<uint64_t> recent_history;
    map<uint64_t, uint64_t> received;

    void init(uint64_t id, uint64_t size, uint8_t fanout, uint8_t ttl, uint8_t window_size, uint64_t pull_delta);
    void receive(message &in, deque<message>& out);
    uint64_t rps();
    uint64_t latency();
    void handle(deque<uint64_t> recv_miss);
};

void node::init(uint64_t id, uint64_t size, uint8_t fanout, uint8_t ttl, uint8_t window_size, uint64_t pull_delta) {
  this->id = id;
  this->fanout = fanout;
  this->ttl = ttl;
  this->size = size;
  this->window_size = window_size;
  this->pull_delta = pull_delta;
  rng.seed(id);
}

uint64_t node::rps() {
  std::uniform_int_distribution<std::mt19937::result_type> dist(0,size-1);
  return dist(rng);
}

uint64_t node::latency() {
  std::uniform_int_distribution<std::mt19937::result_type> dist(10,2000);
  return dist(rng);
}

void node::handle(deque<uint64_t> recv_miss) {
  for (const auto &id : recv_miss) {
    if (received.find(id) != received.end()) continue;
    if (find(missing.begin(), missing.end(), id) != missing.end()) continue;
    missing.push_back(id);
  }
}

void node::receive(message &in, deque<message>& out) {
  switch(in.type) {
    case push: 
      {
        handle(in.recent_history);

        if (received.find(in.id) != received.end()) return;
        received[in.id] = in.received_at;
        recent_history.push_back(in.id);

        if (recent_history.size() > window_size) recent_history.pop_front();

        if (in.ttl > ttl) return;
        message m = in.copy();
        m.sent_at = in.received_at;
        m.received_at = m.sent_at + latency();
        m.ttl += 1;
        m.source = id;
        m.history = recent_history;
        m.missing.clear();

        for (uint8_t i = 0; i < fanout; i++) {
          m.target = rps();
          out.push_back(m);
        }

        break;
      }
    case local:
      {
        message m;
        m.type = local;
        m.source = id;
        m.target = id;
        m.sent_at = in.received_at;
        m.received_at = m.sent_at + pull_delta;
        out.push_back(m);

        m.type = pull;
        m.target = rps();
        m.received_at = m.sent_at + latency();
        m.history = recent_history;
        m.missing = missing;
        out.push_back(m);

        missing.push_front(missing.pop());
        break;
      }
    case pull:
      {
        handle(in.recent_history);
        
        uint64_t sel = std::numeric_limit<uint64_t>::max();
        for (const auto &m : in.missing) {
          if (received.find(m) != received.end()) {
            sel = m;
            break;
          }
        }

        message m = in.copy();
        m.target = in.source();
        m.source = id;
        m.sent_at = in.received_at;
        m.received_at = m.sent_at + latency();
        m.missing.clear();
        m.recent_history = recent_history;
        m.type = reply;
        m.id = sel;

        break;
      }
    case reply:
      {
        handle(in.recent_history);

        if (in.id == std::numeric_limit<uint64_t>::max()) return;
        
        if (received.find(in.id) != received.end()) return;
        received[in.id] = in.received_at;
        recent_history.push_back(in.id);

        if (recent_history.size() > window_size) recent_history.pop_front();

        break;
      }
  }
}

class simu {
  public:
    vector<node> nodes;
    priority_queue<message> q;

    void init(uint64_t count_node);
    void send_messages(uint64_t count_msg, uint64_t delta_t);
};

void simu::init(uint64_t cout_node) {
}

void simu::send_messages(uint64_t count_msg, uint64_t delta_t) {
}

int main(void) {


  return 0;
}
