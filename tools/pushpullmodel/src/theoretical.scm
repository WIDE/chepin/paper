(library
  (src theoretical)
  (export
    push-coverage-pct
    push-coverage-nodes
    messages-exchanged
    push-cost
    usefulness-factor
    pull-cost
    push-pull-cost
    configure-pull
    multi-push-pull-cost)
  (import (chezscheme))

  (define (push-coverage-pct k ttl starting)
    (cond
      ((= ttl 0) starting)
      (#t (- 1 (exp (* -1 k (push-coverage-pct k (- ttl 1) starting)))))))

  (define (push-coverage-nodes k ttl nodes)
    (* (push-coverage-pct k ttl (/ 1 nodes)) nodes))

  (define (messages-exchanged k ttl nodes)
    (* (push-coverage-nodes k (- ttl 1) nodes) k))

  (define (push-cost k ttl nodes)
    (/ (messages-exchanged k ttl nodes) (push-coverage-nodes k ttl nodes)))

  (define (usefulness-factor usefulness)
    (+ 1 (/ (- 1 usefulness) usefulness)))

  (define (pull-cost usefulness single-cost)
    (* single-cost (usefulness-factor usefulness)))

  (define (push-pull-cost k ttl nodes usefulness single-cost)
    (let ([push-cover (push-coverage-pct k ttl (/ 1 nodes))])
      (+
        (* (push-cost k ttl nodes) push-cover)
        (* (pull-cost usefulness single-cost) (- 1 push-cover)))))

  (define (configure-pull k ttl nodes usefulness emit-freq)
    (* 
      (usefulness-factor usefulness)
      emit-freq
      (- 1 (push-coverage-pct k ttl (/ 1 nodes)))))

  (define (multi-push-pull-cost k ttl nodes usefulness single-cost)
    (letrec 
      ([internal
         (lambda (k ttl init-ttl nodes usefulness single-cost)
           (cond
             ((= k 0) '())
             ((= ttl 0) (internal (- k 1) init-ttl init-ttl nodes usefulness single-cost))
             (#t (cons
                   (cons k (cons ttl (push-pull-cost k ttl nodes usefulness single-cost)))
                   (internal k (- ttl 1) init-ttl nodes usefulness single-cost))))
           )])
      (sort
        (lambda (a b) (< (cddr a) (cddr b)))
        (internal k ttl ttl nodes usefulness single-cost))))
  )
