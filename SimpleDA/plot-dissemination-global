#!/bin/bash

if [ "x$1" == "xcompute" ]; then

mkdir -p ./conf_out/
cat > conf_out/conf.yaml <<EOF
IS_CLOCK_GLOBAL: False              # Use a global or logical clock {parameter}
nbNodes : 10000                       # 100 processes {from Figure 6}
probBroadcast : 0.05                # 5% broadcast {from Figure 6}
nbCycles : 100                      # 100 cycles? {Each nodes will broadcast during these number of cycles}
ORDER_MECHANISM: STABLE_ORDER       # EpTO algo (STABLE_ORDER) or regular gossip (SPONTANEOUS_ORDER) {parameter}
NODE_CYCLE: 125                     # delta process round period {from 6. Evaluation introduction}
NODE_DRIFT: 0.01                    # round drift (1% = 1/100 = 0.01) {from 6. Evaluation introduction}
LATENCY_TABLE: data/latencies.obj   # latencies from planetlab {from 6. Evaluation introduction}
LATENCY_DRIFT: None                 # Not used (not implemented) {according to source code}
fanout: 10                          # Fanout (K=?) {2*e*ln(100) / (ln(ln(100))) -> Computed as 16.3938... according to K}
TTL: 20                             # Time to live, {Global: TTL >= (c+1)*log2(n)*(delta_max / delta_min)}
                                    # {Logical: TTL >= 2 * (c+1) * log2(n) * (delta_max / delta_min)}
MEASURE_ORDER: False                # @FIXME Don't know what is it
NETWORK_CODING: False
CHURN : False                       # No churn 
CHURN_RATE : 0.05                   # No churn 
MESSASE_LOSS: 0.0                   # No message loss
PEER_SAMPLING: GLOBAL
EOF

cores=$(grep -c ^processor /proc/cpuinfo)
if [ -n "$2" ]; then
  cores=$2
fi

run=32
echo "using $cores cores for $run run"

#### EpTO dissemination nocode + global view + die

sed -i "s/NETWORK_CODING: [a-zA-Z0-9]*/NETWORK_CODING: False/" ./conf_out/conf.yaml
rm -f conf_out/dissemination-global-nocode-die.txt
for k in {1..20..1}; do

rm -f out.txt
sed -i "s/fanout: [0-9]*/fanout: $k/" ./conf_out/conf.yaml

for run in $(seq 1 $cores $run); do
for core in $(seq 1 1 $cores); do

pypy ./epto-infect-die.py ./conf_out 1 | perl -ne 'print $1 == "0" ? "1\n" : "0\n" if /#procs with missed:  (\d+)/' >> out.txt &

done
wait
done

avg=$(awk '{ total += $1; count++ } END { print total/count }' out.txt)
echo "$k $avg" >> conf_out/dissemination-global-nocode-die.txt

done

#### EpTO dissemination with code + global view + die

sed -i "s/NETWORK_CODING: [a-zA-Z0-9]*/NETWORK_CODING: True/" ./conf_out/conf.yaml
rm -f conf_out/dissemination-global-code-die.txt
for k in {1..20..1}; do

rm -f out.txt
sed -i "s/fanout: [0-9]*/fanout: $k/" ./conf_out/conf.yaml

for run in $(seq 1 $cores $run); do
for core in $(seq 1 1 $cores); do

pypy ./epto-infect-die.py ./conf_out 1 | perl -ne 'print $1 == "0" ? "1\n" : "0\n" if /#procs with missed:  (\d+)/' >> out.txt &

done
wait
done

avg=$(awk '{ total += $1; count++ } END { print total/count }' out.txt)
echo "$k $avg" >> conf_out/dissemination-global-code-die.txt

done

fi



gnuplot --persist <<EOF
n = 10000
m = 50000
f(x) = exp(m * -exp(-(x - log(n))))

#set key outside
set key top left Left width -15
set terminal svg enhanced background rgb 'white'
set terminal svg size 850,500
set output './conf_out/dissemination.svg'

set xrange [0:20]
set yrange [-0.1:1.1]
set xtics 1
set xlabel 'Fanout'
set ylabel 'Successful atomic broadcasts for 500 messages'
set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"
plot f(x) title 'Infect and Die, Store and Forward, Theorical' with lines linestyle 1, \
    './conf_out/dissemination-global-nocode-die.txt' using 1:2 with lines title "Infect and Die, Store and Forward, Sim. Global View", \
    './conf_out/dissemination-global-code-die.txt' using 1:2 with lines title "Infect and Die, Network Coding, Sim. Global View"
EOF
