import uuid, random, os, struct
from pyflexcode import ffi, lib

# Look at src/gf256.h in libflexcode source code
polynom = 29

@ffi.def_extern()
def my_deliver_cb(handle, buf, length):
    return ffi.from_handle(handle).deliver(buf, length)

@ffi.def_extern()
def my_send_cb(handle, packets, length):
    return ffi.from_handle(handle).send(packets, length)

class Pool(object):
    def __init__(self, deliver, send, fanout = 15, maxRank = 0, split = 1, hist = 10):
        assert callable(deliver)
        assert callable(send)

        self.deliver_cb = deliver
        self.send_cb = send

        self.h = ffi.new_handle(self)
        self.poolc = ffi.gc(ffi.new("pool_t*"), lib.pool_free)
        lib.pool_init(self.poolc, self.h, lib.my_deliver_cb, lib.my_send_cb, fanout, maxRank, split, hist, polynom)

    def receive(self, pkt):
        lib.pool_receive(self.poolc, pkt)

    def broadcast(self, msg):
        lib.pool_broadcast(self.poolc, msg, len(msg))

    def deliver(self, msg, length):
        self.deliver_cb(0, ffi.string(msg, length), 0)

    def get_generations_sizes(self):
        res = []
        for i in range(self.poolc.generations.entries_size):
            void_gen = self.poolc.generations.entries[i].data
            genkv = ffi.cast("genkv_t*", void_gen)
            res.append(genkv.known_ids_count)

        return res

    def get_gen_history(self):
        if self.poolc.gen_hist_size == 0: return [0]
        return ffi.unpack(self.poolc.gen_hist, self.poolc.gen_hist_size)

    def send(self, packets, length):
        copied = []
        for i in range(length):
            pp = ffi.gc(ffi.new("pool_packet_t*"), lib.pool_packet_free)
            lib.pool_packet_deep_cpy(pp, ffi.addressof(packets, i))
            copied.append(pp)

        self.send_cb(copied)
