\documentclass[11pt, letterpaper]{article}
\usepackage[margin=0.7in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage[noend]{algpseudocode}
\usepackage{amssymb}
\usepackage{multicol}
\usepackage{graphicx}

\algblockdefx[UPON]{UponReceive}{EndUponReceive}[2]{\textbf{upon receive} \textsc{#1}(#2)}{endupon}
\algblockdefx[TASK]{Task}{EndTask}[1]{\textbf{task} #1}{\textbf{endtask}}

\makeatletter
\ifthenelse{\equal{\ALG@noend}{t}}%
  {\algtext*{EndTask}}
  {}%
\ifthenelse{\equal{\ALG@noend}{t}}%
  {\algtext*{EndUponReceive}}
  {}%
\makeatother

\title{Independant Multi-Sources Network Coding for Efficient Dissemination}
\author{Quentin Dufour \thanks{funded by ANR Grant O'Browser}}
\date{January 2018}

\begin{document}

\begin{titlepage}
\maketitle
\end{titlepage}

\begin{abstract}
Not yet written
\end{abstract}

\twocolumn

We use the following primitive functions:

\begin{itemize}
  \item \textsc{Send}(msg) Take a peer from the RPS service and send it the payload.
  \item \textsc{Deliver}(msg) Deliver to the application a decoded message.
  \item \textsc{UniqueID}() Return a unique ID, such as an UUID (which change for every messages).
  \item \textsc{Rank}(g,set) Count the number of "packets" for the given generation.
  \item \textsc{Decode}(g,set) Take all packets of a given generation, put them in a matrix, apply a gaussian elimination, and replace the old packets with the new one. We can have cases where we take $n$ packets but only put $m, m < n$ back as some packets were linearly dependant.
  \item \textsc{Recode}(g,set) Take all packets of a given generation and recode one packet from these packets.
\end{itemize}


\input{fragments/algo-simple-clock}

Liveness is guaranteed by the fact that when a message with new information is received (ie: after adding the message to the matrix and applying the gaussian elimination, the matrix rank has increased), a random linear combination of the matrix is sent.

Termination is guaranteed by the fact that when a message without new information is received (ie: after adding the message to the matrix and applying the gaussian elimination, the matrix rank remains the same), nothing is done.

With this algorithm, the number of stored message will grow indefinetely.

Here is a proposition with a Garbage Collector:

\input{fragments/algo-simple-clock-gc}

In the article "An Adaptive Peer-Sampling Protocol for Building Networks of Browsers", they propose a broadcast algorithm for Spray (Algorithm 5). This simple algorithm of dissemination could be benchmarked against ours, especially how small the fanout can become in both algorithm, how it perform during churn, how fast messages are delivered between their inital sending and their receiving. We could see how it perform on the real life implementation CRATE.

\section{Network Coding as a middleware between EpTO and Peer Sampling}

We can replace \textsc{Send} by our \textsc{Broadcast} function and \textsc{Deliver} could call \textbf{upon receive} \textsc{Ball}.

We must be careful to the impact on the latency delivery as EpTO is dependant of time. The risk is that we drop more messages than without network coding.

Indeed, the delivery is governed by a Time To Live counter (named $TTL$), which once expired trigger the delivery of the message.
This counter is incremented by two means:

\begin{itemize}
  \item A message is forwarded, it sent version is incremented by one. Messages can be received at any times but are forwarded every $\delta$ times unit. So, the message will be incremented after a time $t_{inc1} \in \left[0,\delta\right]$
  \item A message once received is also kept locally, and is incremented in every loops of the active thread, so after a time $t_{inc2} = \delta$
\end{itemize}

So, we can deduce that a message is delivered after a time $t_{deliver} \in \left[ 0, TTL \times \delta \right]$.

An annoying case could be a message $M_a$ with a clock $c_a = 6$ and a message $M_b$ with a clock $c_b = 12$.
$M_a$ is sent in the $9^{th}$ generation and $M_b$ is sent in the $10^{th}$ generation.
But there is a problem, and a packet is missing to decode the whole $9^{th}$ generation.
After the TTL expiration, $M^b$ is delivered as the $10^{th}$ generation has been decoded.
Finally, the missing packet to decode the $9^{th}$ generation is received, all the messages can be decoded, but will be dropped because $M_b$ has been delivered, and delivering $M_a$ after will break total order. As the $9^{th}$ contains many messages, we can imagine that instead of one dropped message, all the decoded messages could be dropped. That's the difference between with and without network coding: if a packet is missing or received too late, potentially many messages will be dropped.

\newpage
\section{Estimate maximum size of a generation for a given rate}

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{img/latency.png}
\caption{Latency of a message}
\end{figure}

We first want to estimate what is the average latency for a given message in a regular infect and die strategy (without network coding) represented by bars on Figure 1.
The following formula is given by "From Epidemics to Distributed Computing" paper:

\[ \pi' = 1 - e^{- \pi f} \]

So, we know that if we have a proportion of $\pi$ infected process at one round, we will have a proportion of $\pi'$ infected processes at the next round.
We can rewrite it as a sequence:

\[ u(r) = 1 - e^{- f u(r - 1)} \]

If we compute $u(3)$, we will have an estimation of the proportion of infected processes after 3 rounds.
To use this sequence, we must define $u(0)$, which is the round where the message has been emitted.
During this round, only one process knew the message:

\[ u(0) = \frac{1}{n} \]

To compute the average latency of a message dissemination, we don't want the cumulated proportion of infected processes but the proportion of newly infected processes every rounds.
Let's define a new sequence:

\[ v(r) = 1 - e^{- f u(r - 1)} - u(r - 1) \]

Now, we know the number of newly infected processes every rounds. Processes infected at round 0 waited 0 times units, processes infected at round 1 waited in average $L_{avg}$, processes infected at round 2 waited in average $2L_{avg}$, etc. But how many times nodes waited in average? Let $\delta$ be the latency of a message dissemination.

\[ \delta_{avg} = \sum_{r=0}^{\infty} v(r) \cdot r \cdot L_{avg}  \]

Now, if we define a rate in messages per time unit of the whole network, named $w$, we can estimate how many messages were sent during the dissemination of a message thanks to the average dissemination time. Moreover, if we consider that we change to generation $G_{i+1}$ as soon as we receive a message for generation $G_i$, generation size will be the number of messages sent during the average time of a message dissemination:

\[ |G|_{avg} = \delta_{avg} \cdot w \] 

If generations are too small, we define a variable $M$ for max rank. In the following formula we suppose that messages are sent at a regular rate:

\[ |G|_{avg} = \delta_{avg} \cdot w + r \cdot M \] 

If generations are too big we will split nodes in groups. Each groups will encode in distinct generations. The number of groups is determined by $S$:

\[ |G|_{avg} = \delta_{avg} \cdot w \cdot \frac{1}{S} \] 

\onecolumn
\begin{verbatim}
Cumulated infection proportion by round n=100 k=20: 1/100, 0.18126923, 0.97336113, 1.0.
Non cumulated infection proportion by round n=100 k=20: 1/100, 0.17126922, 0.7920919, 0.026638865.
No collision probability for |G| = 400 and 4 bytes id: 0.9999814.
Generation maximum size for n=100, k=20, Lavg=157, rank=8, split=1, rate=0.04: 19.526121.
Generation maximum size for n=1000, k=25, Lavg=157, rank=0, split=4, rate=0.4: 39.465786.
Generation maximum size for n=10000, k=25, Lavg=157, rank=0, split=40, rate=4: 49.56757.
\end{verbatim}
\end{document}

