> 2) In this paper, the authors have evaluated the metrics that are commonly used in the analysis of gossip protocols (i.e. bandwidth, coverage and delay). This is correct but the main idea of this paper (that is using network coding) requires a non negligible amount of computation to be executed in each node. This means that other "host-based metrics" (vs. "network-based metrics") should be also taken in account. For example, the CPU-load of the nodes and the amount of memory that is consumed in each node.

👍 CPU Load and RAM are given in the context of our experiments

> 3) The validation of the proposed approach is based on a simulation-based analysis. Despite of this, the paper does not report any information on the availability (for peer-review or full disclosure) of the Omnet++ simulator that has been developed. Providing the source code of the simulator would permit a much easier repeatability of your results and to peer-review your work. On the other hand, it would also maximize the impact of your paper on the research field.

👍 Everything is open source and links to repos have been added to the paper

> Page 1: "The data sent over the network stays reasonable". What do you mean? This sentence is not clear.

👍 Fixed

> "We run our experiments with 1 000 nodes, sending 1 000 messages at a rate of 150 messages per second". This is not clear. Do you mean 1 000 messages per node or a total amount of 1 000 messages (and therefore 1 message per node)?

👍 Fixed

> what is the network topology?

👍 Fixed. Define that everyone can communicate with everyone

> How does the paper use trace to simulate latency? (Note that latency should depend on packet size. The size of a packet depends on the actual network coding scheme, and therefore can be different from that in the trace.)

👍 After some quick tests, it might be a thing with 100Gb/s networks with jumbo frames, fiber and nano seconds latency. Not in our context or a traditional context. Mentioned in the article

> How does the paper evaluate whether a node can decode a message? Mention in the paper that we are running the RLNC algorithm for real
Does the paper implement Gaussian elimination?

👍 By decoding it. Mention in the paper that we are running the RLNC algorithm for real

> If so, what is the run time for decoding a message? (This is important because there can be many messages in a generation)

👍 Give some performance indications on the time it takes for a simulation to run


> In this regard, wouldn't a simpler solution be to only allow network coding among messages from the same source? Given that a source produces messages at a rate of ~150 messages/sec, there should still be plenty of network coding gains when we only consider network coding among messages from the same source.

👍 Mention 150 msg/sec FOR THE NETWORK + mention 150msg/sec = 1 msg every 6 sec per node

> The paper states "our generations become smaller when the number of messages per second decreases." It is not clear why the statement holds. Lamport timestamp advances by 1 every time there is a local event, such as producing a new message. So, no two messages from the same source can have the same timestamp, and they cannot belong to the same generation. Therefore, it is not clear why the number of messages in a generation has anything to do with the rate of message. This statement actually raises the question on whether the paper implements Lamport timestamp correctly.

👍 Says that it is 1. multi source 2. less message disseminated unknown to the source

> 1. In fact, the number of packets in a generation is never evaluated, either theoretically or numerically, in the paper.
> 2. What is the average number of packets in a generation?

👍 We put the generation's sizes in Figure 5 and explain why its ok / limitations in few lines

> 4) CHEPIN is evaluated in terms of scalability only in the range 100-1000 nodes. This scalability evaluation is quite limited. It would be of interest to evaluate the behavior of the protocol in presence of a massive number of nodes.

❓ More or less answered. Says that it works with 10k nodes but very slow to simulate.

> 5) In this paper, the evaluation of CHEPIN is based on a set of traces. It would be interesting to investigate the behavior of the protocol in different network topologies and with different setups of latencies among the nodes.

❓ More or less answered. I don't see what can be a different network topology, as everyone can communicate with everyone. For latencies, a different distribution could have some impact and is the only improvement I can see but it would take lot of space. Shifting the distribution has been done on Figure 5.

> Why the keyword "gossip" is not in the title of this paper?

❓ Probably too late to change the title?!

> The paper does not address the generation size issue

❓ More or less. We say that if generations are too small, we perform like Pulp. But no solution for too high freq.

> 1) The methodology used to collect the data reported in Section "VI. Evaluation" is not clear. For example, do you have averaged multiple-independent runs? Do you have calculated the confidence intervals of the main metrics that have been analyzed?

❌ Not answered

> Paper is too long

❌ Not yet fixed

> The paper has no deep analytical results.

⚰ [Will not fix] Too bad. I would like to be deep one day...

> The evaluation is weak. While the paper claims to have "experiment results" in several places, it is really Omnet++ simulations with PlanetLab trace.

⚰ [Will not fix] Simulating is not experimenting? Where is the frontier?
