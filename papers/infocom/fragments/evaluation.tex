\section{Evaluation}
\label{sec:evaluation}
We evaluated our solution in the Omnet++ simulator, using traces from
PlanetLab and Overnet to simulate respectively latency and
churn\footnote{Code is accessible at
  https://gitlab.inria.fr/WIDE/chepin/flexnet}.

To assess the effectiveness of \approachName, we implemented a
modified version of Pulp as described in
Section~\ref{sec:application-pulp}, and compare it with the original
Pulp protocol.

% First we present our evaluation methodology, the researched
% properties and present the datasets used.  We then use an empirical
% approach to identify the best configuration for a meaningful
% comparison between our soluimpact of different protocol configurations
% on our researched properties.  We select optimal values for each
% protocol and study the evolution over time of external (delay,
% bandwidth) and internal (linked to the algorithm) metrics to the
% protocol.  Finally, we study these protocols under churn and with
% different network configurations.

\subsection{Experimental setup}

%If not specified, 
Our experiments consist in disseminating 1 000 messages at a rate of
150 messages per second, each message being emitted by a different
nodes amongst the 1 000 nodes in the network. We note that at this
rate, per-node network coding would induce important delays, as 150
msg/sec represents less than one message emitted every 6 seconds per
node.

Every node can communicate with any other node in the network.
Each message weighs 1KB and has a unique identifier.
%For Pulp, it is simply an integer encoded on 8 bytes. In the case of \approachName-Pulp,
%the unique identifier is composed of its generation encoded on
%4 bytes and a unique identifier in this generation, also encoded on 4
%bytes.
$\Delta_{adapt}$ is set to 125 ms.  We consider that the latency
difference that might be induced by the additional headers is
negligible with respect to the payload size and the considered
latency.  The whole RLNC algorithm\footnote{Code is accessible at
  https://gitlab.inria.fr/WIDE/chepin/libflexcode/} was run during the
simulation, including the Gaussian Elimination part, but
encoding/decoding was limited to the coefficients and the first 2
bytes of the payload.  With the previous parameters, a simulation is
run on a single core of an Intel i7-7600U CPU at 2.80GHz in less than
2 minutes using less than 400MB of RAM.
%We did some successful run with 10 000 nodes but stayed with 1 000 nodes for time reasons.
% as the discovery of missing messages will be harder.

% Indeed, we verified that with very small trading window, Pulp cannot
% achieve complete dissemination. 
% achieve an atomic broadcast. Our network coding variant rely less on
% the trading window as it also uses the sparse coefficient vector
% contained in every network coded message to discover missing messages
% too.  For other values, it results only on small variations of delay
% and data used.  It appears that configuring the trading window of our
% protocols with a length of 9 messages and a safety margin of 10 is a
% good compromise.

% \begin{figure}[h]
%   \includegraphics[width=\textwidth]{fragments/graph-planetlab.pdf}
%   \caption{Planetlab latency distribution}
%   \label{fig:planetlab}
% \end{figure}

In order to accurately simulate latency, we use a PlanetLab
trace\cite{zhu_network_2017}. Latency averages at 147 ms with a maximum
of almost 3 seconds. Most of the values (5th percentile and 95th
percentile) are between 20 ms and 325 ms. Finally we have a long tail
of values between 325 ms and the maximum value.

\subsection{Configuring the Protocols}
To configure the protocols, we chose an experimental approach. First,
we selected a suitable value for the size of the trading window.  As
explained in Section~\ref{sec:application-pulp}, too small values of
this parameter result in wasted pull requests, and missing messages,
while too large ones lead to wasted bandwidth.  We therefore tested
the ability of the original Pulp, and of our solution to achieve
complete dissemination (i.e. all messages reach all nodes) with
different trading window sizes, and a safety margin of 10. Results,
not shown for space reasons, show that our solutions reaches complete
dissemination with trading window sizes of at least 6, while Pulp
requires trading-window sizes of at least 9.  For the rest of our
analysis, we therefore considered a trading-window size of 9, and a
safety margin of 10. Nonetheless, this first experiment already hints
at the better efficiency of our network-coding-based solution.

\floatsetup[figure]{style=plain,subcapbesideposition=center}
\begin{figure}[h]
  \sidesubfloat[]{
    \input{fragments/graph-protoconf-1.tex}
    \label{fig:protoconf1}}
  \quad%z

  \sidesubfloat[]{
    \input{fragments/graph-protoconf-2.tex}
    \label{fig:protoconf2}}%

  \caption{Pulp (\ref{fig:protoconf1}) and \approachName-Pulp (\ref{fig:protoconf2}) behavior under various configuration of the protocol (fanout and time to live)}
  \label{fig:protoconf}
\end{figure}

Next, we selected values for fanout and
TTL. Figure~\ref{fig:protoconf} reports the delivery delays and
bandwidth consumption of the two protocols with several values of
these two parameters.  Each measurement has been done 32 times.  Only
the average is reported, the measured values are no more than +/- 5\%
of the average value for the bandwidth and the minimum delay, and no more
than +/- 60\% of the average value for the maximum delay.  To measure
bandwidth consumption, we consider the ratio between the average
amount of bandwidth consumed by the protocol, and the lower bound
represented by the bandwidth required for the same task in a tree
structure in a stable network.  First, we observe that in terms of
delays and bandwidth used, our network-coding variant is more stable
than the original Pulp. That is, with low values of fanout and TTL,
the original algorithm deteriorates faster.

Next, we see that our network-coding variant performs better or
similarly for every combination of fanout and TTL both in terms of
sent bandwidth and delay.  The best configuration in term of sent data
for Pulp corresponds to the configuration $k=6,TTL=4$ with 2.12 KB for
1KB of useful data and an average of 0.67 seconds to disseminate a
message. Our network-coding solution reduces delay to 0.55 s, with a
bandwidth consumption of 1.83KB/msg. With a fanout of $5$, our solution
further decreases consumed bandwidth to 1.66 KB/msg but with a slight
increase in delay (0.83 s). Clearly, to achieve the minimum delays,
the best strategy consists in boosting the push phase by increasing
the TTL, but this defeats the bandwidth-saving goal of Pulp and our
approach.  As a result, we use the configuration with $k=6,TTL=4$ for
both protocols in the rest of our comparison.

\subsection{Bandwidth and delay comparison}


\floatsetup[figure]{style=plain,subcapbesideposition=center}
\begin{figure*}[h]
  \sidesubfloat[]{
    \input{fragments/algo-comparison-orig.tex}
    \label{fig:pulp-comp-orig}}
  \quad%

  \sidesubfloat[]{
    \input{fragments/algo-comparison-nc.tex}
    \label{fig:pulp-comp-nc}}%

  \caption{Comparison of exchanged packet rate, used bandwidth and message delay for \ref{fig:pulp-comp-orig} \textsc{Pulp} and \ref{fig:pulp-comp-nc} \approachName-Pulp}
  \label{fig:pulp-comp}
\end{figure*}

%Now that we have fixed all our protocol parameters, 
We evaluate how our algorithm performs over time in Figure~\ref{fig:pulp-comp}.
First, we logged the number of packets sent per second for the three
types of packets: push, pull and pull reply.  As we configured the two
protocols with the same fanout and TTL, we would expect seeing almost
the same number of push packets. But our network-coded variant sends
12\% more push packets. Pulp stops forwarding a push packet if the
corresponding message is already known. But since our variant can use
a large number of linear combinations, our algorithm manages to
exploit the push-phase better, thereby reducing the number of packets
sent in the pull phase: 33\% fewer pull and pull reply packets. This
strategy enables us to have a packet ratio of only 2.27 % was overhead
                                % of 127\%
instead of 2.70. % was 170\%.

As network coded packets include a sparse vector containing message
identifiers and values, \approachName-Pulp has larger pull and pull
reply packets than Pulp. Considering push packets, we also send more
of them, which explains why we send 17\% more data for these packets.
%However, as our pull phase is way smaller than the one from Pulp, the
%pull reply packets of our algorithm consume 28\% less data than the
%ones from Pulp.  We can also notice that the pull packets consume less
%data than the two others.  Indeed, these packets never contain the 1
%KB payload.  However, we can notice that our algorithm still consumes
%less data than Pulp as we transmit generation id instead of each
%message id.  With 150 messages/second, at a given time time, every
%nodes will be aware of a huge list of missing messages and ask it to
%their peers.  Generally, this list will contain messages sent
%approximately at the same time, so probably in the same generation,
%which explain why it is way more efficient to ask for generation
%identifiers, and why pull packets will be smaller for our algorithm.
At the same time, \approachName-Pulp reduces the header part of pull messages
by asking for generations (groups of message) instead of messages while reducing
the chances of redundancy in replies thanks to RLNC. More generally, the pull phase
is shorter due to a more efficient push phase.
These two facts enable us to have a data ratio 1.84 instead 2.12.

Finally, we study the distribution delay of each message.  As our
algorithm has a longer push phase, delays are shorter on average.  We
see a downward slope pattern on our algorithm's delays, especially on
the maximum-delay part. This pattern can be explained by the fact
that decoding occurs at the end of each generation, so messages that
are sent earlier wait for longer than the most recent ones. 

\subsection{Adaptiveness optimization}

\begin{figure}[h]
  \input{fragments/graph-strat.tex}
  \caption{How adaptiveness algorithms impact the protocol efficiency}
  \label{fig:strat}
\end{figure}

We now carry out a sensitivity analysis to understand the reasons for
our improved performance. To this end, Figure~\ref{fig:strat} compares
\approachName-Pulp identified as Best with Pulp and with two intermediate variants.

The first variant corresponds to a modification in the
\textsc{GetTradingWindow} function of algorithm~\ref{algo3}. Instead
of using the message identifiers contained in the $ids$ variable, we
use the message identifiers contained in the $dlv$ variable like in
the case of the standard Pulp protocol.  In other words, we
disseminate the identifiers of messages we have decoded and not those
we are aware of.

The second variant is a modification of how we count the number of
missing messages at line~\ref{line:compute-missing} in
algorithm~\ref{algo7}. For this variant, we do $missingSize \gets
|missing|$ like in the original Pulp. We thus evaluate the number of
missing messages by counting all the message identifiers we have not
yet decoded, without taking into account the progress of the decoding
in our generations.

The two variants perform worse than our solution both in terms of
delay and bandwidth. Variant 1 does not manage to achieve complete
dissemination with a fanout of 6 and a TTL of 4, while Variant 2
achieves complete dissemination but at a higher cost ratio: 2.4
instead of 1.83 for our solution.  This shows the importance of the
modifications we made to the Pulp protocol.

On Figure~\ref{fig:strat}, we see that Pulp has a better ratio of
useful over useless messages, a smaller pull period and more missing
messages than \approachName-Pulp due to having more messages to pull,
caused by a less efficient push phase.  Best, Variant 1 and Variant 2
have the same push phase, and consequently the same number of messages
to pull.  We see that the pull strategy of Variant 2 is not efficient:
it asks for many messages more frequently with a smaller
useful-over-useless ratio.  Variant 1 performs similarly to Best, but
not better. Moreover its per-disseminated-message efficiency is lower
as it does not provide complete dissemination.

%To understand how these modifications behave, Figure~\ref{fig:strat}
%shows the different between the number of useful and useless packets.
%We see that our algorithm and  Variant 1 perform similarly.  The
%Pulp algorithm has a more efficient pull strategy than ours, possibly
%because there are more messages to pull, we receive redundant linear
%combination for the requested generation or the frequency adaptation
%is not optimal in our case.  However, we see that we obtain way better
%results than the second variant, which mainly pull useless messages.
%At the end, we see lot of useless messages, as we don't inject useful
%messages anymore. The adaptive part of the algorithm therefore
%decreases the number of pull per second, to reduce useless messages.

%When we look at the pull period, we see that Variant 1 and our
%algorithm are quite similar, with the biggest pull period as they have
%less messages to pull.  Pulp has a smaller pull period, but has it has
%more messages to pull, it is not surprising.  Our second variant pull
%period is the smallest even though this variant hasn't more messages
%to pull than our algorithm or our first variant, and explain why we
%have many useless messages: we pull to frequently.

%Finally, when we study the evolution of our missing message estimation, it appears that the
%first variant has the lowest estimation of missing messages. It is due
%to the fact that we relay only decoded messages via the trading
%window, which adds a delay and improve the chances to receive the
%information via network coding coefficients.

% However, it appears that this method is not efficient to efficiently
% disseminate message identifiers as it fails to achieve a complete
% dissemination with high probability. Indeed, if a message is sent at
% the end of a generation, it will only be forwarded by the trading
% window. But if too many people decodes it during the push phase, it
% will not be decoded enough to have its existence enough
% disseminated.


% We see that the second variant has a missing messages estimation far superior to the first variant and our algorithm, close to the Pulp one. This estimation will be used to adapt the pull period, which will be responsible for the usefulness of the pulled messages. It appears that simply counting the number of known missing messages is not efficient in our case.

% \floatsetup[figure]{style=plain,subcapbesideposition=center}
% \begin{figure}[h]
%   \sidesubfloat[]{
%     \input{fragments/graph-strat-nc.tex}
%     \label{fig:strat-nc}}
%   \quad

%   \sidesubfloat[]{
%     \input{fragments/graph-strat-orig.tex}
%     \label{fig:strat-orig}}
%   \quad
  
%   \sidesubfloat[]{
%     \input{fragments/graph-strat-dlv.tex}
%     \label{fig:strat-dlv}}
%   \quad

%   \sidesubfloat[]{
%     \input{fragments/graph-strat-l1.tex}
%     \label{fig:strat-l1}}
%   \quad

%   \caption{Adaptation strategy (\ref{fig:strat-nc} Our algorithm vs \ref{fig:strat-orig} Pulp vs \ref{fig:strat-dlv} Variant 1 vs \ref{fig:strat-l1} Variant 2)}
%   \label{fig:strat-2}
% \end{figure}

% The figure~\ref{fig:strat-2} represents the different strategies used over time to adapt the pull frequency, mainly the chosen condition in the adjustment thread of algorithm~\ref{algo2}.
% The reset strategy correponds to the case where the number of missing messages increases, in this case we recompute the pull period.
% When the number of missing messages is stable or decrease, we decrease the pull period if we receive more useful messages than useless (and we know some missing message identifiers), otherwise we increase the pull period.

% It appears clearly that the second variant reset frequency doesn't work as intended, as nodes always increase the pull period.
% It is a sign that the computed pull period is too small and generate too many useless messages, as seen in figure~\ref{fig:strat}.
% During the message exchange, Pulp only decreases the pull period or reset it, which means that the computed pull period computed during a reset favors uselessness over delays. 
% The two last algorithms behave similarly, with a a mix of reset, increase strategies, and decrease strategies on the second part of the experiment. An equal number of increase and decrease would mean a balanced number of useful and useless messages.
% %In the case of Pulp, we see only the reset strategy at the begining because the number of known missing messages increase slower than others algorithm (due to the lack of network coding coefficients).

\subsection{Behaviour under network variation}

\begin{figure}[h]
  \input{fragments/graph-network.tex}
  \caption{Variation of network configuration. The number near each
    point indicates the average generation size for NC.}
  \label{fig:variation-netconfig}
\end{figure}

Figure~\ref{fig:variation-netconfig} shows how our algorithm performs
under different network configurations and gives the average
generation size for each point. 
%We see that when the number of messages per second decreases, the difference in term of data sent between the algorithms decreases too.
We observe that the difference between \approachName-Pulp and Pulp is
greater with larger message rates, longer latency, and larger
networks, while being correlated with generation sizes. For a given
latency distribution, larger generations tend to lead to more
efficient coding thereby improving performance. In particular,
generations become larger when there are more messages being
disseminated in the network at approximately the same time as is the
case when increasing latency, message rate, or network size.
 
 %   Indeed, metadata like generations and
% messages identifiers are more disseminated than payloads, and make
% better use of low-latency links, which explains why generations are
% smaller with the PlanetLab distribution.  Better performances could be
% explained by other mechanisms of the protocol that could also take
% advantage of the fast metadata dissemination.

% If our algorithm slightly decrease its bandwidth usage, the Pulp increase can be explained by three reasons even if the push phase is the same regardless the sending rate
% First, at any given time, we know more missing messages identifiers when the rate is higher, which cause bigger pull packets as they will contain more message identifiers.
% Second, due to the fixed $\Delta_{adjust}$, higher the message rate is, higher the number of events to process in the adjustment thread at once will be, and particularly the variation of missing messages, which will be bigger, implying smaller pull periods and more messages sent, similarly to the second variant in figure~\ref{fig:strat}.
% Finally, the defined "safety margin" could be insufficient, and we could pull messages that are still in the push phase.

% On the same principle, when latency become smaller, generations
% become smaller too, as the number of messages with the same clock is
% dependant of the latency and the rate of the network. We see that
% our algorithm scale with the network.

At one extreme, when we have only one message per generation, we have
the same model as Pulp: asking for a list of generation identifiers is
similar to asking for a list of message identifiers in Pulp. At the
other extreme, the risk is to have too many messages per generation,
impacting the node's local resources.  However, we see that the
generation size increases logarithmically, as when we have more
messages per generation, we also have more messages to disseminate the
knowledge of this generation. 

Figure~\ref{fig:variation-netconfig} also displays the results
obtained with a uniform latency distribution with a lower bound set to
the $5^{th}$ percentile of the PlanetLab dataset and an average
similar to the one from the PlanetLab dataset, resulting in an upper
bound of 274ms. We observe that \approachName-Pulp's improvement over
Pulp is greater with the PlanetLab distribution, even if this means
smaller generations.

%We also note that \approachName-Pulp parameters are less dependant of the network configuration than Pulp, as running both algorithm on 8 500 nodes with optimal parameters chosen for 1 000 nodes shows that our algorithm uses twice less data than Pulp.

%The network
%coded packets will contain a generation identifier, a sparse vector
%containing only one message identifier and one coefficient and the
%message.  As a generation identifier plus a message identifier of our
%algorithm has the same size of a message identifier in Pulp, the only
%overhead is the one byte coefficient value.

% \begin{figure}[h]
%   \input{fragments/graph-overnet.tex}
%   \caption{Overnet active nodes}
%   \label{fig:overnet}
% \end{figure}

We use an Overnet trace to simulate
churn\cite{bhagwan_understanding_2003}. The trace contains more than
600 active nodes over a total of 900 with continuous churn---around
0.14143 events per second.

\begin{figure}[h]
  \input{fragments/graph-churn.tex}
  \caption{Delay observation with churn on the system}
  \label{fig:churn}
\end{figure}

We use this trace replayed at different speeds to evaluate the impact
of churn on the delivery delays of our messages, as plotted on
Figure~\ref{fig:churn}.  Specifically, we consider three speeds: 500,
1000 and 2000 times faster for respectively 71, 141 and 283 churn
events per second.  We see that the original Pulp algorithm is not
affected by churn, as the average and maximum delivery delays stay
stable and similar to those without churn.  Considering the average
delay, it's also the case for our algorithm: the average delay does
not evolve. The maximum delay does not evolve significantly
either. However we can see huge differences in the shape of the
maximum delay for each individual message. Indeed, the decoding order
and the generation delimitation are affected by churn, but this has
limited impact on message dissemination.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: ../paper.tex
%%% End: 
