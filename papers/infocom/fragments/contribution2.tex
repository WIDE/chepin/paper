\section{Contribution}

\subsection{System model}
Our model consists of a network of $n$ nodes that all run the same
program. These nodes communicate over a unicast unreliable and fully
connected medium, such as UDP over the Internet. Nodes can join and
leave at any moment, as no graceful leave is needed; crashes are
handled like departures.  We consider that each node can obtain the
addresses of some other nodes in the system via a Random Peer Sampling
service~\cite{RPS}. There is no central authority to coordinates
nodes: all operations are fully decentralized and all exchanges are
asynchronous.  We use the term message to denote the payload that must
be disseminated to every node of the network, and the term packet to
denote the exchanged content between two nodes.  We consider the
exchange of multiple messages and any node in the network can inject
messages in the network, without any prior coordination between nodes
(messages are not pre-labeled).

\subsection{Solving RLNC challenges}

In our multiple-independent-source model and with our goal to cover
push and pull protocols, we propose new solutions to the previously
stated challenges.

\label{sec:solving-rlnc-challenges}

\paragraph{Assigning generations with Lamport timestamps}
With multiple senders, we need to find a rule that is applicable based
only on the knowledge of a single node when assigning a generation as
relying on the network would be costly, slow and unreliable.  We chose
Lamport timestamps~\cite{lamport1978time} to delimit generations by
grouping every message with the same clock in the same generation.
This method doesn't involve sending new packets as the clock is
piggybacked on every network-coded packet.  When a node wants to
disseminate a message, it appends its local clock to the packet and
updates it, when it receives a message, it uses its local clock and the
message clock to update its clock.  This efficiently associates
messages that are disseminated at the same time to the same
generation. % Indeed, we know that packets in the same generation $g_n$
% were sent during the time interval of a full dissemination in the
% network.  Otherwise, one or more of these packets would have been sent
% to the next generation $g_{n+1}$ as the emitting generation $g_n$
% would have been known to the emitting node, which would have updated
% its generation.

\paragraph{Sending coefficients in sparse vectors}
When multiple nodes can send independent messages, they have no clue
of which identifiers are assigned by the other nodes.  Consequently,
they can only rely on their local knowledge to choose their
identifiers.  Choosing identifiers randomly on a namespace where all
possible identifiers are used would lead to conflicts.  That is why we
decided to use a bigger namespace, where conflict probabilities are
negligible when identifiers are chosen randomly.  On a namespace of
this size, it is impossible to send a dense vector over the network,
but we can send a sparse vector: instead of sending a vector
$c^1,...,c^n$, we send $m$ tuples, corresponding to the known
messages, containing the message id and their coefficient:
$(id(M^{i_1}), c^{i_1}),...,(id(M^{i_m}), c^{i_m})$.

% Could be only an engineering problem?
\iffalse
Internally, it is easier to manipulate dense vectors,
that's why on a message reception, we recreate a dense vector with identifiers we know and keep a map between the dense vector position and their correspond identifiers, so our local dense vector's size corresponds to the number of known message identifiers and not the size of the namespace.
Each node has its own maps, containing different messages identifiers, in variable orders.
After that, this dense vector and its associated data will be appended to the decoding matrix.
Next, we can perform a Gaussian elimination on the matrix to try to decode new messages.
We compile all these steps in a function named \textsc{Solve}.
On a node, when a previously unknown message identifier is received, the matrix must be updated to add a zeroed column corresponding to the new message identifier coefficients before appending the newly received data.
The inverse operation is realized when a message is about to be created with \textsc{Recode}.
We create a new random linear combination from our matrix.
Then, the generated dense encoding vector is converted to a sparse encoded vector containing the message identifier and its value thanks to the map between our local dense vector and messages identifiers.
\fi

\paragraph{Pulling generations instead of messages}
A node sends a list of generations that it has not fully decoded to its neighbors. The target node answers with one of the generation it knows.
To determine if the information will be redundant, there is no other solution than asking the target node to generate a linear combination and try to add it to the node's local matrix.
\iffalse
Asking specific identifiers would be useless as, if the remote node answers only if it has decoded the identifiers, we loose the benefits of RLNC.
Otherwise, if the remote node answers as soon as it has some knowledge of the requested message, nothing proves that this knowledge is not redundant with the asking node's one.
\fi
During our tests,
we observed that blindly asking for generations did not increase the number of redundant
packets compared to a traditional Push-Pull algorithm asking for a
list of message identifiers, while greatly decreasing message sizes.

\paragraph{Count needed independent linear combinations}
To provide adaptiveness, some protocols need to estimate the number of
useful packets needed to receive all the missing messages.  Without
network coding, the number of needed packets corresponds to the number
of missing messages.  With network coding, partially decoded packets
are also considered as missing messages, but to decode them we need
fewer packets than missing messages.  In this case, the number of
required useful packets corresponds to the number of required
independent linear combinations.
%By substracting the rank of a generation to the total number of message identifiers known for this generation, we can determine the number of linear combination we need for this generation.
%We just apply this rule for all the known generations and report this value.

\subsection{CHEPIN}

\input{fragments/algo-handler2}

To ease the integration of RLNC in gossip-based dissemination algorithms, we encapsulated some common logic in algorithm~\ref{algo3}.
% This procedure aims to be as general as possible to be reusable when converting any dissemination algorithms to RLNC.
We represent a network-coded packet by a triplet: $\langle g, c, e
\rangle$, where $g$ is the generation number, $c$ an ordered set
containing the network-coding coefficients and $e$ the encoded
payload.

%The coefficient set contains one or more tuple with the following structure: $\langle id, val \rangle$ where $id$ is the message identifier and $val$ is the value of the coefficient.

We define 3 global sets: $rcv$, $ids$, and $dlv$. $rcv$ contains a
list of network-coded packets, as described before, which are modified
each time a new one is received to stay linearly independent until all
messages are decoded. $ids$ contains a list of known message
identifiers under the form $\langle g, gid \rangle$ where $g$ is the
generation and $gid$ is the identifier of the message inside the
generation. By using this tuple as a unique identifier, we can reduce
the number of bytes of $gid$ as the probability of collision inside a
generation is lower than the one in the whole system. Finally $dlv$
contains a list of message identifiers similar to $ids$, but contains
only identifiers of decoded messages.

The presented procedure relies on some primitives. \textsc{Rank} returns the rank of the generation, by counting the number of packets associated to the given generation. \textsc{Solve} returns a new list of packets after applying a Gaussian elimination on the given generation and removing redundant packets.
%\textsc{Gen} returns the generation corresponding to the given message identifier.
\textsc{Deliver} is called to notify a node of a message (if the same message is received multiple times, it is delivered only once).

This procedure updates the 3 previously defined global sets, delivers
decoded messages and returns the usefulness of the given packet.
Internally, the node starts by adding the packet to the matrix and by
doing a Gaussian elimination on the packet's generation (line
\ref{line:decode}), if decoding the packet does not increase the
matrix rank, the packet is deemed useless and the processing stops
here.  Otherwise, the node must add unknown message identifiers from
the packet-coefficient list to the known-identifier set.  After that,
the node delivers all the  messages decoded thanks to the received
packet and stores their identifiers in $dlv$.  Finally, the node
checks if the clock must be updated.

Algorithms~\ref{algo-push-nc} and~\ref{algo-pull-nc} show how the
above procedures can be used to implement push and pull gossip
protocols.  For push, we do not directly forward the received packet,
but instead forward a linear combination of the received packet's
generation after adding it to our received-packet list. For Pull, we
request generations instead of messages.  Like existing protocols, we
keep a $rotation$ variable that rotates the set of missing
identifiers, allowing missing generations to be generated in a
different order on the next execution of the code block.

\input{fragments/algo-push2}

\input{fragments/algo-pull2} 

