\section{Concepts}

\subsection{Random Linear Network Coding (RLNC)}
% Network Coding: An Instant Primer

\begin{figure}[h]
\begin{tikzpicture}

% nodes
\node (A) at (0, 2) {A};
\node (B) at (0, 0) {B};
\node (C) at (2.8, 1) {C};
\node (D) at (8, 2) {D};
\node (E) at (8, 0) {E};

  \path[->, every node/.style={sloped,anchor=south}]
  (A) edge node[style={anchor=north,pos=.3}]{$M^1$} (C) 
  (A) edge node[style={anchor=north}]{$M^1$} (D)
  (B) edge node[style={anchor=south,pos=.3}]{$M^2$} (C) 
  (B) edge node{$M^2$} (E)
  (C) edge node[style={anchor=north,pos=.75}]{$\mathbf{c^1}, c^1_1 M^1 + c^1_2 M^2$} (D)
  (C) edge node[style={anchor=south,pos=.75}]{$\mathbf{c^2}, c^2_1 M^1 + c^2_2 M^2$} (E);

\end{tikzpicture}
  \caption{With RLNC, C can send useful information to D and E without knowing what they have received}
  \label{fig:rlnc}
\end{figure}

RLNC\cite{fragouli_network_2006} is a way to combines different packets inside a network to improve their dissemination speed by increasing the chance that receiving nodes learn something new. In figure~\ref{fig:rlnc}, the node C can't know what D and E have received. By sending a linear combination of $M^1$ and $M^2$, the two original messages, nodes D and E can respectively recover $M^2$ from $M^1$ and the linear combination and $M^1$ from $M^2$ and the linear combination.
Without RLNC, node C would have to send both $M^1$ and $M^2$ to D and E involving the sending of two more packets.
We consider that every packets consist of $L$ bits. To handle packets of different size, we can split or pad the packet.

The packet content has to be split as symbols over a field
$\mathbb{F}_{2^n}$. $\mathbb{F}_{2^8}$ field has interesting properties when doing RLNC.
First, a byte can be represented as a symbol in this field. Thereafter, this
field is small enough to speed up some computing with discrete
logarithms tables and at the same time sufficiently large to guarantee
linear independency of the random coefficients with very high
probability.

Encoded packets are linear combination over $\mathbb{F}_{2^n}$ of multiple packets. This linear combination is not a concatenation, if original packets are of size $L$, encoded packets will be of size $L$ too.
This encoded packet carries a part of the information of all the original packets, but not enough to recover any original packet. After receiving enough packets, the original packets will be decodable.

To perform the encoding, we consider $n$ original packets $M^1,...,M^n$ known by all sources.
Each time we want to create an encoded packet, a sequence of coefficients $c_1,...,c_n$ is chosen randomly. The encoded packet $X$ is computed from the original packets and the coefficients as follow: $X = \sum_{i=0}^{n} c_i M^i$. An encoded packet is constitued by a sequence of coefficients and the encoded information: $(c, X)$.

Every participating nodes can recursively encode new packets from the one they received, including packets that are not yet decoded.
If we consider a node that received $(c^1, X^1),...,(c^m, X^m)$ encoded packets, a new packet $(c',X')$ can be encoded by choosing a random set of coefficients $d_1,...,d_m$, computing the new encoded information $X' = \sum_{j=1}^{m} d_j X^j$ and computing the new sequence of coefficients $c_i' = \sum_{j=1}^{m} d_j c^j_i$.

An original packet $M^i$ can be considered as an encoded packet by creating a coefficient vector $0,...,1,..,0$ where 1 is at the ith position. We can see the encoding as a subset of the recursive encoding technique, allowing us to implement only the second one.

But there are two reasons to limit $n$, the number of packets that are encoded together.
First, Gauss-Jordan elimination has a $O(n^3)$ complexity, which becomes rapidly too expensive to compute.
Then, more we encode packets together, bigger the sequence of coefficients will be while the encoded information remains stable, resulting in extreme cases in sending mainly coefficients on the network instead of information.
To encode as many data as we want, we split messages in groups named generations. Only messages in the same group are encoded together.

\subsection{RLNC with sparse vectors}

When multiple nodes are enabled to send independent messages, they have no clue on which ids are assigned by the other nodes.
Consequently, they can only rely on their local knowledge to choose their identifiers.
Choosing identifiers randomly on a namespace where all possible identifiers are used would lead to conflicts.
However, we decided to use a bigger namespace, where conflict probabilities are neglictible when identifiers are chosen randomly.
On a namespace of this size, it is impossible to send a dense vector over the network, however we can send a sparse vector: instead of sending a vector $c^1,...,c^n$, we send $m$ tuples, corresponding to the known messages, containing the message id and their coefficient: $(id(M^{i_1}), c^{i_1}),...,(id(M^{i_m}), c^{i_m})$.

% Could be only an engineering problem?
Internally, it is easier to manipulate dense vectors,
that's why on a message reception, we recreate a dense vector with identifiers we know and keep a map between the dense vector position and their correspond identifiers, so our local dense vector's size corresponds to the number of known message identifiers and not the size of the namespace.
Each node has its own maps, containing different messages identifiers, in variable orders.
After that, this dense vector and its associated data will be appended to the decoding matrix.
Next, we can perform a gaussian elimination on the matrix to try to decode new messages.
We compile all these steps in a function named \textsc{Solve}.
On a node, when a previously unknown message identifier is received, the matrix must be updated to add a zeroed column corresponding to the new message identifier coefficients before appending the newly received data.

The inverse operation is realized when a message is about to be created with \textsc{Recode}.
We create a new random linear combination from our matrix.
Then, the generated dense encoding vector is converted to a sparse encoded vector containing the message identifier and its value thanks to the map between our local dense vector and messages identifiers.

\subsection{RLNC with Lamport timestamps generations}
To limit the decoding complexity, we need to allow encoding between messages of the same group.
As for coefficients vectors, we need to find a rule that is applicable only with the knowledge of a single node.
We chose Lamport timestamps to delimit generations, and particularly to group every messages with the same clock on the same generation.
When a node wants to disseminate a message, it appends its local clock to the packet and update it, when it receives a message, it uses its local clock and the message clock to update its clock.
This method is efficient to encode messages that are disseminated at the same time in the network, to limit delay and increase efficiency.
Indeed, we know that packets in the same generation $g_n$ were sent during the time interval of a full dissemination in the network.
Otherwise, one or more of these packets would have been sent to the next generation $g_{n+1}$ as the emitting generation $g_n$ would have been known to the emitting node, which would have updated its generation.

%To work, RLNC needs same length messages.
%However, we don't need to proactively pad short messages, at least we can set a maximum size after which messages are cut in two or more messages.

%If\df{Why if? is this what we do or not?} the message length is stored inside the message or the message is terminated by a distinctive symbol (like 0), we can pad encoded packets allowing to lazily pad messages. If two messages have different size, the shortest one is padded with its last byte. We can't pad with random values or fixed values, as these values will prevent the decoding of the message. This technique allows us to save bytes when all messages are short, preventing us from padding every messages to a standard
%size for nothing.
