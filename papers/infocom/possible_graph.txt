# Pulp

## Discrete simulation on 100 nodes

### Figure 1 - push and pull behaviour

identify useless patterns

### Figure 2 - identify tensions between complete dissemination and redundant values

 * x=ttl y=fanout z=coverage (how many nodes received the message)
 * x=ttl y=fanout z=complete disseminations (atomic broadcast)
 * x=ttl y=fanout z=useless packets received

--> it could be interesting to compare these figures to our network coding implementation
Indeed, network coding performs complete dissemination way sooner and probably useless packets comes later. But we need to change how data are graphed as it works only with multiple messages dissemination.

## Real world implementation on local cluster

Dissemination of 200 messages in a 1001 nodes network

### Figure 4 - delay and usefulness of messages

 * x=time y=number of nodes that received a given message --- one line per message
 * x=time y=delays --- lines for max, 90th, 75th, 50th, 25th, 5th perc.
 * x=time y=pull replies/peer/sec --- lines useful and useless

###  Figure 5 - Reception latency considering initial push phase

 * x=push coverage+fanout y=delay -- bars (max, 90, 75, 50, 25, 5th perc.)
 * x=push coverage+fanout y=push duplicates

### Figure 6 - delay evolution under churn

Replay of the Overnet file sharing network join and departure.

 * x= time, y=update time distribution --- with different speed of replay

What is update time distribution?
Simulated with 650 nodes.

## Planetlab

### Figure 7 --> Message delay of sending 200 messages
 
 * x=time of publication y=time of reception

### Figure 8 --> Adaptiveness + usefulness when message injection rate change

 * x=time y=pull replies/peer/second
 * x=time y=pulling period (1 / freq) distribution
 * x=time y=message frequency injection

### Figure 9 --> reaction to message burst

 * x=time y=pull replies/peer/second
 * x=time y=pulling period distribution
 * x=time y=messages sent

### Figure 10 --> some strange experiments to compare with push-only and pull only
