\documentclass[slideopt,A4,handout]{beamer}
\usepackage[utf8]{inputenc}
\useoutertheme{infolines}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{blkarray}
\usepackage{xcolor}
\usepackage[backend=biber,style=authortitle,autocite=footnote,doi=false,isbn=false,url=false]{biblatex}
\usepackage{eso-pic}
%\usepackage{monster2e}
\usepackage{tikz}
\usepackage{layout}
\usepackage{xcolor}
\usepackage{amsmath,amssymb}
\usepackage{fancybox}
\usepackage[absolute,showboxes,overlay]{textpos}
\TPshowboxesfalse
\textblockorigin{0mm}{0mm}
\usetheme{Boadilla}
\usecolortheme{beaver}

\addtobeamertemplate{footnote}{\vspace{-16pt}\advance\hsize-0.5cm}{\vspace{16pt}}
\makeatletter
\renewcommand*{\footnoterule}{\kern -3pt \hrule \@width 2in \kern 16pt}
\definecolor{rouge2}{RGB}{230,68,57}  % red S
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}
\addbibresource{biblio.bib}
\setlength{\parskip}{\baselineskip}

\setbeamercolor{bibliography entry author}{fg=rouge2}
\setbeamercolor{bibliography entry journal}{fg=rouge2}
\setbeamercolor{bibliography entry note}{fg=rouge2}

\renewcommand{\sfdefault}{lmss}
\sffamily

\setbeamersize{text margin left=1cm,text margin right=1cm}

\title{Multisource Rumor Spreading\\ with Network Coding}
\author[Quentin Dufour]{Quentin Dufour\\supervised by David Bromberg and Davide Frey}\institute{WIDE team at INRIA Rennes}

\begin{document}

\setbeamertemplate{background canvas}{\includegraphics[width=\paperwidth,height=\paperheight]{template/premiere-sc-fr}} 

\setbeamertemplate{footline}{ \hspace{5em} \textcolor{white} {Quentin Dufour \hfill\today}\hspace{2em}\null \vspace*{3pt}}

\begin{frame}

\begin{textblock*}{9cm}(13mm,50mm)
{\textcolor{white} {
{\huge Multisource Rumor Spreading}\\[2mm]
   { \huge with Network Coding}\\[3mm]
   	{\large supervised by David Bromberg and Davide Frey}}
	}
	\end{textblock*}


\vspace*{-4pt}
\end{frame}

%\begin{frame}
%  \titlepage
%\end{frame}

\setbeamertemplate{background canvas}{\includegraphics[width=\paperwidth,height=\paperheight]{template/basrouge}} 

\setbeamertemplate{footline}{
\hspace{2cm} 
\raisebox{2.5ex}
  {\textcolor{white}{Quentin Dufour - Multisource Rumor Spreading with Network Coding}}\hfill 
  \raisebox{2.5ex}
  {\textcolor{white}{\today - \insertframenumber \hspace{5mm} \null }}}




\begin{frame}
  \frametitle{Context: Probabilist Total Order with EpTO}
  \begin{columns}
  \begin{column}{.5\textwidth}
    \includegraphics[width=\textwidth]{epto.png}
  \end{column}
  \begin{column}{.5\textwidth}
    \begin{description}[align=left]
      \item [Total Order] Used for State Machine Replication. Found in Consul, etcd, zookeeper...
      \item [EpTO\footnotemark] Probabilistic agreement: small probability that a message is not delivered
    \end{description}
  \end{column}
  \end{columns}

  \footcitetext{matos_epto:_2015}
\end{frame}

\begin{frame}
  \frametitle{Problem: EpTO uses lot of bandwidth}
	\[ \text{R} = \frac{\text{transferred data}}{\text{delivered data}}\]
	For 100 nodes and 500 messages: \textbf{R = 300}  \\
    With packets of 1KB: $\sim$\textbf{150MB/node} to get \textbf{0.5MB of data}.

    \normalsize Bandwidth is often:
    \begin{itemize}
      \item expensive (Data transfer costs \textdollar0.087/GB on Azure\footnote{https://azure.microsoft.com/en-us/pricing/details/bandwidth/})
      \item limited (ADSL upload is in average 0.15 MB/s in France\footnote{http://www.ariase.com/fr/vitesse/observatoire-debits.html})
    \end{itemize}

    \Large Can we use less bandwidth?
\end{frame}


\begin{frame}
  \frametitle{Introduction: Gossip and Dissemination}
  
  \begin{columns}
  \begin{column}{.5\textwidth}
    \begin{description}[align=left]
      \item [Gossip] Each nodes exchange information with its neighbours
      \item [Dissemination] Send a message to everyone in the network
      \item [Complete Dissemination] Every nodes in the network have received the message
    \end{description}
  \end{column}
  \begin{column}{.5\textwidth}
    \includegraphics[width=\textwidth]{gossip2.png}
	  \[n = 6\]
  \end{column}
  \end{columns}
\end{frame}

\iffalse
\begin{frame}
  \frametitle{Context: Coupon's Collector Problem}

    \begin{center}
    \includegraphics[width=0.9\textwidth]{coupon-collector.png} \\
	    \textit{Simulation\footcite{euster_epidemics_2004} with $n=50 000$ (Infect and Die, Push)}
    \end{center}

\end{frame}
\fi

\begin{frame}
  \frametitle{Related Works: Dissemination}
  
  \setlength{\parskip}{\baselineskip}
  ($\text{R} = \frac{\text{transferred data}}{\text{delivered data}}$)
  \begin{description}[align=left]
	  \item [Balls and Bins\footnotemark] Used by EpTO. Loose bounds. R = 300.
	  \item [Infect and Die\footnotemark] Theoretical results. R = 25. 
    \item [Push and Pull\footnotemark] Improvement on only pull or push strategy.
  \end{description}

  Only for a single message dissemination \\
  \Large Any solution for multiple messages?
  \addtocounter{footnote}{-3}
  \stepcounter{footnote}\footcitetext{koldehofe_simple_2004}
  \stepcounter{footnote}\footcitetext{euster_epidemics_2004}
  \stepcounter{footnote}\footcitetext{felber_pulp:_2012}
\end{frame}

\begin{frame}
  \frametitle{Introduction: Network Coding}
  \[ n = 5, |m| = 2 \]
  \begin{columns}
  \begin{column}{.4\textwidth}
    \begin{center}
    \includegraphics[width=0.8\textwidth]{butterfly1.png}

    8 packets
    \end{center}
  \end{column}
  \begin{column}{.4\textwidth}
    \begin{center}
    \includegraphics[width=0.8\textwidth]{butterfly3.png}
    
    6 packets
    \end{center}
  \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Introduction: Network Coding}

  \begin{columns}
  \begin{column}{.3\textwidth}
  \[ n = 5, |m| = 2 \]
  \begin{center}
    \includegraphics[width=\textwidth]{butterfly2.png}

  6 packets
  \end{center}
  \end{column}
  \begin{column}{.7\textwidth}

  \setlength{\parskip}{\baselineskip}
  
  \begin{columns}
  \begin{column}{.5\textwidth}
  \setlength{\parskip}{\baselineskip}
  Node D

  $\begin{pmatrix}
    1 & 0 & m1 \\
    3 & 5 & e1 
  \end{pmatrix}$

$\begin{cases} 1m_1 + 0m_2 = m_1 \\ 3m_1+5m_2=e_1 \end{cases}$

\end{column}
\begin{column}{.5\textwidth}
\setlength{\parskip}{\baselineskip}
Node E

  $\begin{pmatrix}
    0 & 1 & m2 \\
    7 & 2 & e2 
  \end{pmatrix}$

$\begin{cases} 0m_1 + 1m_2 = m_2 \\ 7m_1+2m_2=e_2 \end{cases}$
\end{column}
\end{columns}

\Large Messages numbering? \\
\Large Limit equation size?  \\
\Large Limit coefficients to send? \\
\Large Theoretical bounds?
\end{column}
\end{columns}

\end{frame}

\begin{frame}
\frametitle{Related Works: Network Coding}

\begin{columns}
\begin{column}{.5\textwidth}
\textbf{Theoretical}: Rounds needed for sequential sending\footnotemark:
\begin{itemize}
  \item $\mathcal{O}(|m|\log(n))$ no coding
  \item $\Theta(|m|+\log(n))$ coding
\end{itemize}
\end{column}

\begin{column}{.5\textwidth}
\textbf{Practical}: Generations\footnotemark aim to limit the size of the equation to solve / coefficients to send.
\end{column}
\end{columns}
\includegraphics[width=\textwidth]{file-netcode.png} \\
\Large And with events instead of files?   
\addtocounter{footnote}{-2}
 \stepcounter{footnote}\footcitetext{haeupler_analyzing_2011}
 \stepcounter{footnote}\footcitetext{chou_practical_2003}
\end{frame}

\begin{frame}
\frametitle{Contribution: how to define generations?}

  \begin{description}
    \item [Handle generations with Lamport timestamps] ~\\
    When a packet is received: $C_{local} = max(C_{local}, C_{packet})$ \\
    When generation size increases: $|G_{C_{local}}| \geq S_{gen} \Rightarrow C_{local}++ $
    Example with $S_{gen} = 2$
  \end{description}
    \includegraphics[width=0.9\textwidth]{netcode-v1.png}
  
\end{frame}

\begin{frame}
  \frametitle{Contribution: how to handle coefficents?}
	\textbf{Coefficients encoding and matrix building}


    \begin{columns}
    \begin{column}{.5\textwidth}
	    Each message has a unique ID ($m_i \neq m_j \Rightarrow id_{m_i} \neq id_{m_j}$)

	\[\text{1\textsuperscript{st} packet: } (\{id_{m_9}: 5, id_{m_2}: 8\}, e_1) \]
        \[ G = 
	  \begin{blockarray}{ccc}
	  id_{m_9} & id_{m_2} & pl \\
	  \begin{block}{(ccc)}
	  5 & 8 & e1 \\
	  \end{block}
	\end{blockarray}
	\]


  \end{column}
    \begin{column}{.5\textwidth}
	    \[\text{2\textsuperscript{nd} packet: } (\{id_{m_4}: 240\}, e_2) \]
	\[ G = 
	\begin{blockarray}{cccc}
	  id_{m_9} & id_{m_2} & id_{m_4} & pl \\
          \begin{block}{(cccc)}
	  5 & 8 & 0 & e1 \\
          0 & 0 & 240 & e2 \\
	\end{block}
	\end{blockarray}
	\]

	\[\text{3\textsuperscript{rd} packet: } (\{id_{m_4}: 3, id_{m_2}: 4\}, e_3) \]
	\[ G = 
	\begin{blockarray}{cccc}
	  id_{m_9} & id_{m_2} & id_{m_4} & pl \\
          \begin{block}{(cccc)}
	  5 & 8 & 0 & e1 \\
	  0 & 0 & 240 & e2 \\
	  0 & 4 & 3 & e3 \\
	\end{block}
	\end{blockarray}
	    \]

  \end{column}
  \end{columns}
\end{frame}

\begin{frame}
	\frametitle{Contribution: how to choose unique IDs?}
	\setlength{\parskip}{\baselineskip}\textbf{Random IDs} For a generation $G$ and IDs encoded on $b$ bytes:
	\[\mathbb{P}(\text{no collision}) \geq e^{-\frac{|G|(|G|-1)}{2^{8b}}}\]
	With $|G| \leq 20$, we can encode IDs on 4 bytes.
        

	\textbf{Overhead of 10\% in the worst case} \\
	With $|G| = 20$, 4 bytes IDs and a payload of 1KB.\\
	A packet: $(\textcolor{red}{gen}, \textcolor{red}{\{id_x: y, id_{x'}: y', ...\}}, \textcolor{green!50!black}{payload})$

\end{frame}

\begin{frame}
  \frametitle{Evaluation}
    \begin{description}
      \item [Simulator] Using SimpleDA (EpTO's simulator)
    \end{description}
    \begin{center}
    \includegraphics[width=0.7\textwidth]{dissemination-netcode.png}

	    \textit{For the same probability of complete dissemination, 5 less neighbours are needed ($n=100$ and $|m|=500$)}
    \end{center}
\end{frame}

\begin{frame}
  \frametitle{Conclusion \& Future work}
    
    \textbf{Conclusion} 
    \begin{itemize}
      \item Improve multiple messages dissemination
      \item Intuition that can be paired with existing dissemination protocols
    \end{itemize}

    \textbf{Future work} 
    \begin{itemize}
      \item Better management of generation sizes
      \item Quantify improvement
      \item Test with other dissemination algorithms
      \item Real world implementation
    \end{itemize}
\end{frame}

\end{document}
