## Meeting 2018-07-05

We study two potential optimization for Pulp:

 * We should study if, instead of taking a fixed size part of the recent history of a node, we could take a time slice.
   Example : we could put in the trading window, tall the messages received during the last 5 seconds or between 6 seconds and 2 seconds if we want to keep a safety margin
   This time slice could be **later** dynamically adapted. We could include only messages that are known for less than the observed max or mean dissemination time.

 * To replace the safety margin that should prevent disseminating message ids that are still pushed, we could only add messages that are received and reached their max ttl.

